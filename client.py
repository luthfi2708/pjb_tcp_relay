# -*- coding: utf-8 -*-
"""
Created on Thu Oct 04 23:21:33 2018

@author: ipi
"""

# Echo client program
import socket

#HOST = '202.95.132.53'    # The remote host
#PORT = 6001              # The same port as used by the server

HOST = 'ds.manvis.web.id'    # The remote host
#HOST = 'localhost'    # The remote host
PORT = 4274              # The same port as used by the server
s = None

def client(ip, port, message):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((ip, port))
    try:
        sock.sendall(bytes(message, 'ascii'))
        response = str(sock.recv(1024), 'ascii')
        print("Received: {}".format(response))
    finally:
        sock.close()
        
client(HOST,PORT,'tes')