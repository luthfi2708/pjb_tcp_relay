# -*- coding: utf-8 -*-
"""
Created on Thu Oct 04 23:16:36 2018

@author: ipi
"""
# a simple tcp server
import socket
import logging
logging.basicConfig(filename='/var/www/html/argpjb/logger.txt',
                    level=logging.DEBUG,
                    format='%(asctime)s : %(message)s',
                    filemode='a')
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  
sock.bind(('43.252.137.132', 4274))  
sock.listen(5)  
try:
    while True:  
        connection,address = sock.accept()  
        buf = connection.recv(1024)  
        print(repr(buf))
        logging.info(repr(buf))
        connection.send(buf)    		
        connection.close()
except(KeyboardInterrupt):
    print("Killed")
    sock.close()
