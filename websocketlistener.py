# -*- coding: utf-8 -*-
"""
Created on Tue Oct 16 14:07:02 2018

@author: ipi
"""


from flask import Flask, request, make_response, abort
from flask_restful import Resource, Api
from flask_cors import CORS
from flask_compress import Compress
import logging
logging.basicConfig(filename='/var/www/html/argpjb/logger.txt',
                    level=logging.DEBUG,
                    format='%(asctime)s : %(message)s',
                    filemode='a')

app = Flask(__name__)
COMPRESS_MIN_SIZE = 1
COMPRESS_LEVEL = 6
COMPRESS_MIMETYPES = [
'text/html',
'text/css',
'text/xml',
'application/json',
'application/javascript'
]
Compress(app)
api = Api(app)
cors = CORS(app)

dislaimer = """HTTP listener.
Author : Luthfi imanal satrya
Email  : luthfi.imanal@gmail.com"""

class echoserver(Resource):
    def get(self):
        return make_response(dislaimer)
    
    def post(self):
        try:
            data = request.data
            logging.info(repr(data))
            return make_response(data)
        except(IOError):
            abort(404)
        except:
            raise
            
            
api.add_resource(echoserver,'/echo')

@app.route('/')
def hello():
    return dislaimer

app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False
    
if __name__ == '__main__':
#    app.run(host= '0.0.0.0', port= 27083, debug= True, threaded=True)
    app.run(host= '0.0.0.0', port= 27084, debug= True)