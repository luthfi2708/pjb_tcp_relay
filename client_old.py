# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 16:18:03 2019

@author: luthf
"""

# Echo client program
import socket
import sys

#HOST = '202.95.132.53'    # The remote host
#PORT = 6001              # The same port as used by the server

HOST = 'ds.manvis.web.id'    # The remote host
PORT = 4274              # The same port as used by the server
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
    af, socktype, proto, canonname, sa = res
    try:
	s = socket.socket(af, socktype, proto)
    except socket.error, msg:
	s = None
	continue
    try:
	s.connect(sa)
    except socket.error, msg:
	s.close()
	s = None
	continue
    break
if s is None:
    print 'could not open socket'
    sys.exit(1)
s.send('tes masuk\r\n')
#data = s.recv(1024)
s.close()
#print 'Received', repr(data)