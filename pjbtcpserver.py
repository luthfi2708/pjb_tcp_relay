
import threading
import socketserver
import time
import paho.mqtt.client as mqtt

savefile = '/var/www/html/argpjb/logger.txt'

class ThreadedTCPRequestHandler(socketserver.BaseRequestHandler):

    def handle(self):
        data = str(self.request.recv(1024), 'ascii')
        with open(savefile,'a') as fsv:
            fsv.write(data)
        print('Received: {}'.format(repr(data)))
        pjbparser(data)
        response = bytes("ok", 'ascii')
        self.request.sendall(response)

class ThreadedTCPServer(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

def pjbparser(data):
    spt = data.strip().split(',')
    if spt[0] == '$KG-ARG0':
        argmqtt(spt)
    if spt[0] == '$KG-WQC0':
        pass
    else:
        pass
    
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload))

def argmqtt(datalist):
    staid=datalist[1]
    dtsf='%s-%s-%s %s:%s:%s' % (datalist[2][4:],datalist[2][2:4],
                                datalist[2][0:2],datalist[3][0:2],
                                datalist[3][2:4],datalist[3][4:6])
    rr = datalist[4]
    status = datalist[5]
    memory = '%s%%' % datalist[6]
    if datalist[7] == 0:
        door = 'open'
    else:
        door = 'close'
    
    client = mqtt.Client()
    client.username_pw_set('aws', 'aws123')
    client.on_connect = on_connect
    client.on_message = on_message
    
    client.connect("localhost", 1883, 60)
    client.loop_start() #start loop to process received messages
    client.publish('argpjb/%s/date' % staid, payload=dtsf, qos = 1, retain=True)
    client.publish('argpjb/%s/rr' % staid, payload=rr, qos = 1, retain=True)
    client.publish('argpjb/%s/status' % staid, payload=status, qos = 1, retain=True)
    client.publish('argpjb/%s/memory' % staid, payload=memory, qos = 1, retain=True)
    client.publish('argpjb/%s/door' % staid, payload=door, qos = 1, retain=True)
    client.disconnect() #disconnect
    client.loop_stop() #stop loop


if __name__ == "__main__":
    # Port 0 means to select an arbitrary unused port
    HOST, PORT = "0.0.0.0", 4274

    server = ThreadedTCPServer((HOST, PORT), ThreadedTCPRequestHandler)
    ip, port = server.server_address

    # Start a thread with the server -- that thread will then start one
    # more thread for each request
    server_thread = threading.Thread(target=server.serve_forever)
    # Exit the server thread when the main thread terminates
    server_thread.daemon = True
    server_thread.start()
    print("Server loop running in thread: ", server_thread.name)
    print("Server loop port in thread: ", port)

    try:
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        print("killed")
        server.shutdown()